require 'fluent/test'
require 'fluent/plugin/out_unit_time_filter'
require 'tempfile'
require 'time'

# Disable Test::Unit
module Test::Unit::RunCount; def run(*); end; end

RSpec.configure do |config|
  config.before(:all) do
    Fluent::Test.setup
  end
end

def tempfile(content, basename = nil)
  basename ||= "#{File.basename __FILE__}.#{$$}"

  Tempfile.open(basename) do |f|
    f << content
    f.flush
    f.rewind
    yield(f)
  end
end

def run_driver(options = {})
  options = options.dup

  filter = options[:filter] || (<<-EOS)
proc {|rs|
  {
    'count'   => rs.count,
    'inspect' => rs.inspect
  }
}
  EOS

  option_keys = [
    :unit_sec,
    :prefix,
    :emit_each_tag,
    :pass_hash_row,
    :hash_row_time_key,
    :hash_row_tag_key,
  ]

  additional_options = option_keys.map {|key|
    if options[key]
      "#{key} #{options[key]}"
    end
  }.join("\n")

  tag = options[:tag] || 'test.default'

  tempfile(filter, options[:tempfile]) do |f|
    fluentd_conf = <<-EOS
type unit_time_filter
filter_path #{f.path}
#{additional_options}
    EOS

    driver = Fluent::Test::OutputTestDriver.new(Fluent::UnitTimeFilterOutput, tag).configure(fluentd_conf)

    driver.run do
      yield(driver)
    end
  end
end
