require 'fluent/plugin/unit_time_filter_buffer'

class Fluent::UnitTimeFilterOutput < Fluent::Output
  Fluent::Plugin.register_output('unit_time_filter', self)

  unless method_defined?(:log)
    define_method("log") { $log }
  end

  config_param :filter_path,       :type => :string
  config_param :unit_sec,          :type => :size,   :default => 1
  config_param :prefix,            :type => :string, :default => 'filtered'
  config_param :emit_each_tag,     :type => :bool,   :default => false
  config_param :pass_hash_row,     :type => :bool,   :default => false
  config_param :hash_row_time_key, :type => :string, :default => 'time'
  config_param :hash_row_tag_key,  :type => :string, :default => 'tag'

  BUFFER_KEY = :unit_time_filter_buffer

  def configure(conf)
    super

    unless File.exist?(@filter_path)
      raise Fluent::ConfigError, "No such file: #{@filter_path}"
    end

    begin
      @filter = Object.new.instance_eval(File.read(@filter_path), @filter_path)
    rescue => e
      raise Fluent::ConfigError, "Invalid filter: #{@filter_path}: #{e}"
    end

    unless @filter.respond_to?(:call)
      raise Fluent::ConfigError, "`call` method not implemented in filter: #{@filter_path}"
    end
  end

  def shutdown
    super
    Thread.current[BUFFER_KEY] = nil
  end

  def use_buffer
    buf = Thread.current[BUFFER_KEY]

    unless buf
      buf = Buffer.new({
        :filter            => @filter,
        :unit_sec          => @unit_sec,
        :prefix            => @prefix,
        :emit_each_tag     => @emit_each_tag,
        :pass_hash_row     => @pass_hash_row,
        :hash_row_time_key => @hash_row_time_key,
        :hash_row_tag_key  => @hash_row_tag_key,
        :log               => log,
      })

      Thread.current[BUFFER_KEY] = buf
    end

    begin
      yield(buf)
    rescue Exception => e
      Thread.current[BUFFER_KEY] = nil
      log.error(e)
      raise e
    end
  end

  def emit(tag, es, chain)
    use_buffer do |buffer|
      buffer.resume(tag, es)
    end

    chain.next
  end
end
